#ifndef UNORDERED_MAP_HPP
# define UNORDERED_MAP_HPP

#  pragma once

#  include <utility>
#  include <sstream>
#  include <functional>
#  include <vector>
#  include <list>
#  include <optional>

template <	class Key,
			class T,
			class Hash = std::hash<Key>,
			class KeyEqual = std::equal_to<Key>
		 >
class unordered_map {
	public: /* member types */
		using key_type =		Key;
		using mapped_type =		T;
		using value_type =		std::pair<key_type, mapped_type>;
		using reference =		value_type&;
		using pointer =			value_type*;
		using const_reference =	const value_type&;
		using size_type =		std::size_t;
		using hasher =			Hash;
		using bucket =			std::list<value_type>;

	private: /* private fields */
		static const size_type	_default_capacity = 4;

		std::vector<bucket>	_table;
		hasher				_hasher;
		size_type			_capacity;
		size_type			_size;

	private: /* private member functions */
		void	_expand() {
			unordered_map	expanded(_capacity * 2);
			for (auto it = begin(); it != end(); ++it)
				expanded.insert(*it);
			*this = expanded;
		}

		void	_reduce() {
			unordered_map	reduced(_capacity / 2);
			for (auto it = begin(); it != end(); ++it)
				reduced.insert(*it);
			// *this = reduced;
		}

	public: /* member functions */
		unordered_map()
		: _capacity(_default_capacity)
		, _size(0)
		{
			for (size_type i = 0; i < _capacity; ++i)
				_table.push_back(std::list<value_type>());
		}

		unordered_map(size_type capacity)
		: _capacity(capacity)
		, _size(0)
		{
			for (size_type i = 0; i < _capacity; ++i)
				_table.push_back(std::list<value_type>());
		}

		unordered_map(unordered_map const& m)
		: _table(m._table)
		, _hasher(m._hasher)
		, _capacity(m._capacity)
		, _size(m._size)
		{}

		unordered_map(unordered_map&& m)
		: _table(m._table)
		, _hasher(m._hasher)
		, _capacity(m._capacity)
		, _size(m._size)
		{}

		~unordered_map() {}

		unordered_map&	operator=(unordered_map const& m) {
			_table = m._table;
			_hasher = m._hasher;
			_capacity = m._capacity;
			_size = m._size;
			return *this;
		}

		unordered_map&	operator=(unordered_map&& m) {
			_table = m._table;
			_hasher = m._hasher;
			_capacity = m._capacity;
			_size = m._size;
			return *this;
		}

		void	insert(value_type const& value) {
			_table[_hasher(value.first) % _capacity].push_back(value);
			if ((float)_size / (float)_capacity >= 0.75)
				_expand();
			++_size;
		}

		void	clear() noexcept {
			_table.clear();
			_size = 0;
			_capacity = _default_capacity;
			for (size_type i = 0; i < _capacity; ++i)
				_table.push_back(std::list<value_type>());
		}

		size_type	size() const noexcept {
			return _size;
		}

		mapped_type&		at(const key_type& k) {
			size_t hashkey = _hasher(k) % _capacity;

			for (auto& i : _table[hashkey])
				if (i->first == k)
					return i->second;
			throw std::out_of_range("Invalid key");
		}

		const mapped_type&	at(const key_type& k) const {
			size_t hashkey = _hasher(k) % _capacity;

			for (auto const& i : _table[hashkey])
				if (i->first == k)
					return i->second;
			throw std::out_of_range("Invalid key");
		}

		mapped_type&		operator[](const key_type& k) noexcept {
			size_t hashkey = _hasher(k) % _capacity;

			for (auto& i : _table[hashkey])
				if (i->first == k)
					return i->second;
		}

		const mapped_type&	operator[](const key_type& k) const noexcept {
			size_t hashkey = _hasher(k) % _capacity;

			for (auto const& i : _table[hashkey])
				if (i->first == k)
					return i->second;
		}

		mapped_type&	value(const key_type& k, const mapped_type& defval) {
			try {
				return at(k);
			}
			catch (std::out_of_range& e) {
				return defval;
			}
		}

		std::vector<key_type>	keys() noexcept {
			std::vector<key_type>	keys;

			for (auto it = begin(); it != end(); ++it)
				keys.push_back(it->first);
			return keys;
		}

		std::string	toJSON() {
			std::stringstream	ss;
			bool				is_first = true;

			ss << "{";
			for (auto it : *this) {
				if (!is_first)
					ss << ", ";
				ss << "\"" << it.first << "\" : \"" << it.second << "\"";
				is_first = false;
			}
			ss << "}";
			return ss.str();
		}

	public: /* iterators */
		class	iterator {
			private:
				typename std::vector<bucket>::iterator		it_v;
				typename bucket::iterator					it_l;
				bool	is_end;

				unordered_map& map;

			public:
				iterator(typename std::vector<bucket>::iterator another_it_v,
						unordered_map& map) : map(map) {
					it_v = another_it_v;
					if (it_v == map._table.end()) {
						is_end = true;
					}
					else {
						is_end = false;
						it_l = it_v->begin();
					}
				}

				bool	operator==(iterator const& it) const noexcept {
					if (it.is_end && is_end)
						return true;
					if (it.it_v == it_v && it.it_l == it_l)
						return true;
					return false;
				}

				bool	operator!=(iterator const& it) const noexcept {
					return !(it == *this);
				}

				reference	operator*() const {
					return *it_l;
				}

				pointer		operator->() const {
					return &(*it_l);
				}

				iterator&	operator++() {
					++it_l;
					if (it_l == it_v->end())
						++it_v;
						if (it_v == map._table.end()) {
							is_end = true;
							return *this;
						}
					it_l = it_v->begin();
					return *this;
				}
		};

	public: /* iterators */
		iterator	begin() noexcept {
			return iterator(_table.begin(), *this);
		}

		iterator	end() noexcept {
			return iterator(_table.end(), *this);
		}
};

#endif /* UNORDERED_MAP_HPP */
