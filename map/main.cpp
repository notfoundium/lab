#include "unordered_map.hpp"

#include <iostream>

int main()
{
	unordered_map<int, int> m;
	m.insert({0, 1});
	m.insert({1, 1});
	m.insert({2, 2});

	std::cout << m.toJSON() << std::endl;

	for (auto it : m)
		std::cout << it.first << " : " << it.second << std::endl;

	return 0;
}