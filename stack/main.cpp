#include "stack.hpp"

#include <iostream>

int main()
{
	try {
	stack<int>	s = {1, 2, 3};

	s << 1;
	std::cout << s;
	}
	catch (std::logic_error e) {
		std::cerr << e.what() << std::endl;
	}
	return 0;
}