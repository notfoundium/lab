#ifndef MY_STACK_HPP
# define MY_STACK_HPP

# pragma once

#  include <cstddef>
#  include <cassert>
#  include <stdexcept>
#  include <istream>

template <class T>
class stack {
	/* Member types */
	public:
		using value_type =		T;
		using reference =		value_type&;
		using const_reference =	const value_type&;
		using pointer =			value_type*;
		using size_type =		size_t;

	/* Private members */
	private:
		value_type*	_arr;
		size_type	_size;
		size_type	_max_size;

	/* Member functions */
	public:
		explicit stack() noexcept : _arr(nullptr), _size(0), _max_size(0) {}

		explicit stack(stack const& s)  {
			*this = s;
		}

		explicit stack(stack&& s) noexcept {
			*this = s;
		}

		stack(std::initializer_list<value_type> list)
		: _arr(nullptr)
		, _size(0)
		, _max_size(0)
		{
			for (auto it = list.begin(); it != list.end(); ++it)
				this->push(*it);
		}

		stack&	operator=(stack const& s) {
			if (this == &s)
				return *this;
			if (this != &s && _arr != nullptr) {
				delete _arr;
				_arr = nullptr;
			}
			_max_size = s._max_size;
			_size = s._size;
			_arr = new value_type[_max_size];
			assert(_arr);
			for (size_type i = 0; i < _size; ++i)
				_arr[i] = s._arr[i];
		}

		stack&	operator=(stack&& s) noexcept {
			if (this != &s && _arr != nullptr) {
				delete _arr;
				_arr = nullptr;
			}
			_max_size = s._max_size;
			_size = s._size;
			_arr = s._arr;
			s._arr = nullptr;
		}

		~stack() noexcept {
			if (_arr != nullptr) {
				delete [] _arr;
				_arr = nullptr;
			}
		}


		bool			empty() const noexcept {return _size == 0;}

		size_type		size() const noexcept {return _size;}

		void			push(const value_type& val) {
			if (_size + 1 >= _max_size)
				realloc_array();
			_arr[_size++] = val;
		}

		const_reference	top() const {
			if (_size < 1)
				throw std::logic_error("Container is empty");
			else
				return _arr[_size - 1];
		}

		pointer			pop() {
			if (_size < 1)
				throw std::logic_error("Container is empty");
			else
				return (new value_type(_arr[--_size]));
		}

		void			clear() noexcept {
			if (!_arr)
				return ;
			delete [] _arr;
			_arr = nullptr;
			_size = 0;
			_max_size = 0;
		}

		stack&	operator<<(const_reference value) {
			this->push(value);
			return *this;
		}

		stack&	operator>>(pointer value) {
			value = this->pop();
			return *this;
		}

	/* Non-member functions overload */

	friend std::ostream&	operator<<(std::ostream& os, stack const& s) {
		if (s._size < 1)
			throw std::logic_error("Container is empty");
		for (size_type i = 0; i < s._size; ++i)
			os << s._arr[s._size - i - 1] << std::endl;
		return os;
	}

	/* Private member functions */
	private:
		void	realloc_array() {
			value_type*	tmp = _arr;

			if (_max_size == 0) {
				_max_size = 1;
				_arr = new value_type[_max_size];
				assert(_arr);
				return ;
			}
			_max_size *= 2;
			_arr = new value_type[_max_size];
			assert(_arr);
			for (size_type i = 0; i < _size; ++i)
				_arr[i] = tmp[i];
			delete [] tmp;
		}
};

#endif /* MY_STACK_HPP */
