#include "Tree.hpp"

int main()
{
	Tree<int> tree;

	tree.add(42);
	tree.add(21);
	tree.add(84);
	tree.add(1);
	tree.add(0);

	// tree.remove(21);
	std::cout << tree << std::endl;

	return 0;
}