#include <iostream>
#include <stack>
#include <initializer_list>
#include <mutex>

template <class T>
class Tree {
	public: /* iterator prototypes */
		class	Aiterator;
		class	iterator_lnr;
		class	const_iterator_lnr;
		class	const_iterator_lrn;
		class	const_iterator_nlr;

	public: /* member types */
		using value_type =		T;
		using reference =		value_type&;
		using const_reference =	value_type const&;
		using size_type =		std::size_t;
		using iterator =		iterator_lnr;
		using const_iterator =	const_iterator_lnr;

	private: /* Nested node class */
		class Node {
			public: /* member functions */
				Node(const_reference value) noexcept
				: _value(value)
				, _left(nullptr)
				, _right(nullptr)
				{}

				value_type&	get_value() noexcept {
					return _value;
				}

				Node*&		get_left() noexcept {
					return _left;
				}

				Node*&		get_right() noexcept {
					return _right;
				}

				void	set_value(const_reference value) {
					_mutex.lock();
					_value = value;
					_mutex.unlock();
				}

				void	set_left(const_reference value) {
					_mutex.lock();
					_left = new Node(value);
					_mutex.unlock();
				}

				void	set_right(const_reference value) {
					_mutex.lock();
					_right = new Node(value);
					_mutex.unlock();
				}

			private: /* fields */
				value_type	_value;
				Node*		_left;
				Node*		_right;
				std::mutex	_mutex;
		};

	private: /* fields */
		Node*		_root;

	private: /* member functions */
		void	_add_node(Node*& node, const_reference value) {
			if (!node) {
				node = new Node(value);
				return ;
			}
			else if (value > node->get_value() && !node->get_right())
			{
				node->set_right(value);
				return ;
			}
			else if (value < node->get_value() && !node->get_left())
			{
				node->set_left(value);
				return ;
			}
			else if (value > node->get_value() && node->get_right())
			{
				_add_node(node->get_right(), value);
				return ;
			}
			else if (value < node->get_value() && node->get_left())
			{
				_add_node(node->get_left(), value);
				return ;
			}
			return ;
		}

		void	_delete_node(Node*& node) {
			if (node->get_left())
				_delete_node(node->get_left());
			if (node->get_right())
				_delete_node(node->get_right());
			delete node;
			node = nullptr;
		}

	public: /* member functions */
		Tree() noexcept
		: _root(nullptr)
		{}

		Tree(Tree const& tree) : Tree() {
			for (const_iterator it = tree.cbegin(); it != tree.cend(); ++it)
				add(*it);
		}

		Tree(Tree&& tree) : Tree() {
			_root = tree._root;
		}

		Tree(std::initializer_list<T> list) : Tree() {
			for (auto it = list.begin(); it != list.end(); ++it)
				add(*it);
		}

		~Tree() {
			if (_root)
				_delete_node(_root);
		}

		void	add(const_reference element) {
			_add_node(_root, element);
		}

		bool	find(const_reference element) const noexcept {
			for (auto it = begin(); it != end(); ++it)
				if (*it == element)
					return true;
			return false;
		}

		Tree	sub(const_reference element) {
			if (!find(element))
				return Tree();
			Node*	node = _root;
			Tree	tmp;

			while (node->get_value() != element) {
				if (element > node->get_value())
					node = node->get_right();
				else
					node = node->get_left();
			}

			for (const_iterator it(node); it != cend(); ++it)
				tmp.add(*it);

			return Tree(tmp);
		}

		/* В этой части кода те, кто умеют читать, могут об этом пожалеть */
		void	remove(const_reference element) {
			if (!find(element))
				return ;
			Node*	node = _root;
			Tree	tmp(*this);

			_delete_node(_root);
			for (auto it = tmp.cbegin(); it != tmp.cend(); ++it)
				if (*it != element) {
					add(*it);
				}
		}

		void	clear() noexcept {
			if (_root)
				_delete_node(_root);
		}

		/* non-member functions */

		friend std::ostream& operator<<(std::ostream& os, Tree const& tree) {
			for (auto it = tree.cbegin(); it != tree.cend(); ++it)
				std::cout << *it << " ";
			return os;
		}

	public: /* iterators */
		class	Aiterator {
			public:
				Aiterator() = default;
				virtual Aiterator& operator++() = 0;


				bool	operator==(Aiterator const& it) const noexcept {
					return current == it.current;
				}

				bool	operator!=(Aiterator const& it) const noexcept {
					return current != it.current;
				}

			protected: /* fields */
				Node*				current;
				std::stack<Node*>	stack;

			protected: /* member functions */
				void	move_left() {
					stack.push(current);
					current = current->get_left();
				}

				void	move_right() {
					stack.push(current);
					current = current->get_right();
				}

		};

		class	Aconst_iterator : public Aiterator {
			public:
				const_reference operator*() {
					return this->current->get_value();
				}
		};

		class	iterator_lnr : public Aiterator {
			public:
				iterator_lnr(Node* node) {
					if (!node)
						this->current = nullptr;
					else {
						this->current = node;
						while (this->current->get_left())
							this->move_left();
					}
				}

				reference operator*() {
					return this->current->get_value();
				}

				iterator_lnr& operator++() {
					if (this->current->get_right()) {
						this->stack.push(this->current);
						this->current = this->current->get_right();
						while (this->current->get_left()) {
							this->stack.push(this->current);
							this->current = this->current->get_left();
						}
						return *this;
					}
					else {
						while (!this->stack.empty()) {
							Node* prev = this->current;
							this->current = this->stack.top();
							this->stack.pop();
							if (prev == this->current->get_left())
								return *this;
						}
						this->current = nullptr;
						return *this;
					}
				}
			};

		class	const_iterator_lnr : public Aconst_iterator {
			public:
				const_iterator_lnr(Node* node) {
					if (!node)
						this->current = nullptr;
					else {
						this->current = node;
						while (this->current->get_left())
							this->move_left();
					}
				}

				const_iterator_lnr& operator++() {
					if (this->current->get_right()) {
						this->stack.push(this->current);
						this->current = this->current->get_right();
						while (this->current->get_left()) {
							this->stack.push(this->current);
							this->current = this->current->get_left();
						}
						return *this;
					}
					else {
						while (!this->stack.empty()) {
							Node* prev = this->current;
							this->current = this->stack.top();
							this->stack.pop();
							if (prev == this->current->get_left())
								return *this;
						}
						this->current = nullptr;
						return *this;
					}
				}
		};

		// class	const_iterator_lrn : public Aconst_iterator {
		// 	public:
		// 		const_iterator_lrn(Node* node) {
		// 			if (!node)
		// 				this->current = nullptr;
		// 			else {
		// 				this->current = node;
		// 				while (this->current->get_left())
		// 					this->move_left();
		// 				while (this->current->get_right())
		// 					this->move_right();
		// 			}
		// 		}

		// 		const_iterator_lrn& operator++() {
		// 			while (!this->stack.empty()) {
		// 				Node* prev = this->current;
		// 				this->current = this->stack.top();
		// 				this->stack.pop();
		// 				if (prev == this->current->get_right())
		// 					return *this;
		// 			}
		// 		}
		// };

		class	const_iterator_nlr : public Aconst_iterator {
			public:
				const_iterator_nlr(Node* node) {
					if (!node)
						this->current = nullptr;
					else {
						this->current = node;
					}
				}

				const_iterator_nlr& operator++() {
					if (this->current->get_left()) {
						this->stack.push(this->current);
						this->current = this->current->get_left();
						return *this;
					}
					go_right:
					if (this->current->get_right()) {
						this->stack.push(this->current);
						this->current = this->current->get_right();
						return *this;
					}
					while (!this->stack.empty()) {
						Node* prev = this->current;
						this->current = this->stack.top();
						this->stack.pop();
						if (prev == this->current->get_left())
							goto go_right;
					}
					this->current = nullptr;
					return *this;
				}
		};

	public: /* iterators */
		iterator	begin() const noexcept {
			return iterator(_root);
		}

		iterator	end() const noexcept {
			return iterator(nullptr);
		}

		const_iterator	cbegin() const noexcept {
			return const_iterator(_root);
		}

		const_iterator	cend() const noexcept {
			return const_iterator(nullptr);
		}
};
